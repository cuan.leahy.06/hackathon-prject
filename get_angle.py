import math
from hiper import hiper  # import the function


def get_angle(N, fs, d_micro):
    for ele in N:
        if ele != 0:  # Find all sounds that came in at an angle
            j = 0.1  # Steps
            x = [round(z * j, 1) for z in range(-200, 201)]  # x-axis
            [y1] = hiper(ele, x, -d_micro / 2, fs)  # call hiper
            x1 = round(len(x) / 4)
            x2 = round(len(x) / 8)
            pendiente = ((y1[x1] - y1[x2]) / j * (x1 - x2))
            if ele > 0:
                angulorad = math.atan(pendiente)
                angulo1 = angulorad*180/math.pi
                angle = -90-angulo1
            else:
                angulorad = math.atan(-pendiente)
                angulo1 = angulorad * 180 / math.pi
                angle = 90-angulo1
        else:
            angle = 0
        return angle
