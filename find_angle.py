import math
import numpy as np
import time as t
from f_adap import f_adap
from get_angle import get_angle

# CONSTANT VALUES
fs = 44100  # Sampling frequency in Hz
d_micro = 0.1  # distance between microphones in metres
c = 340  # speed of sound in (m/s)
muestrasMAX = math.ceil((d_micro*fs)/c)  # Maximum number of samples, Nmax
DESP = math.ceil(muestrasMAX*1.5)
lengData = fs*(d_micro*5)
signal1 = wavread('90.wav')  # importing file to process


# LOOP WHERE THE DIFFERENT PARTS OF THE IMPORTED FILE ARE PROCESSED
for k = lengData:lengData:len(signal1):
    t.time()  # measure of time
    signal = signal1(1, k-(lengData-1):k)
    d = signal1(2, k-(lengData-1):k)


# NORMALISATION PROCESS
M1 = max(abs(signal)) # Maximum of channel 1
M2 = max(abs(d)) # Maximum of channel 2
M3 = max(M1, M2) # Normalisation value
d = 2*d/M3 # Normalising


# LMS ALGORITHM
hDESP = [np.zeros(1, DESP) 1] # Filter to delay the signal DESP samples
d1 = np.convolve(hDESP, d)
P = 50 # Parameters of the algorithm
mu = 0.0117
h0 = np.zeros(1, P) # Initialising the adaptive filter
h0[1] = 0
h, y, e = f_adap(signal, d1, h0, mu) # Recursive function calculating the coefficients of the filter h(n)


# PROCESSING THE FILTER BEFORE THE FREQUENCY ANALYSIS
h1 = [np.zeros(1,DESP-muestrasMAX-3), h[DESP-muestrasMAX-2:len(h)]]
h1[DESP+muestrasMAX+2:len(h1)] = 0
h1[DESP+1]= h1[DESP+1]/2
[B,I]= sorted(h1, reverse = True)
H1=[np.zeros(1, I(1)-3), h[I(1)-2:I(1)+2], np.zeros(1,len(h)-(I(1)+2))]

# FREQUENCY ANALYSIS TO OBTAIN THE DELAY (IN SAMPLES)
lh = 128 # Length of the FFT
'''H = np.fft.fft(h1, lh)''' # H=fft(h1,lh);

# 2-ANGLE(+UNWRAP)
'''idk tbh''' # alpha=angle(fftshift(H));
# q=unwrap(angle(fftshift(H)));

# SLOPE
'''M = diff(q)''' #smth to do with the approximate derivative of q... at... some... point?

# SLOPES AVERAGE
lM = len(M) + 2
p1 = np.floor((lM / 2) - 4)
p2 = np.ceil((lM / 2) + 4)
K = mean()

