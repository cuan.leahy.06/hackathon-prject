import numpy as np


def f_adap(x, d, h, mu):
    P = len(h)
    N = len(x)
    y = np.zeros(1, N)
    e = y  # Reserve space for y[] y e[]
    rP = sorted([z for z in range(-P+1, 0)], reverse = True)
    for k in range(P, N):
        xx = x[k + rP[k-P]]  # Last P inputs x[k], x[k-1], ... x[k-P]
        print(xx)
        y[k] = xx*h  # Filter output: x*h Convolution
        e[k] = d[k] - y[k]  # Error
        h += mu*e[k]*xx  # We update the filter coefficients
    return h, y, e

f_adap([1,2,3,4,5], [1,2,3,4,5], [0], 0.1)

