import numpy as np
import matplotlib.pyplot as plotter
def fourier_transform(amplitude):  # function returns the values of the fourier transform for a given frequency
    fourierTransform = np.fft.fft(amplitude)/len(amplitude)
    fourierTransform = fourierTransform[range(int(len(amplitude)/2))]
    tpCount = len(amplitude)
    values = np.arange(int(tpCount/2))
    timePeriod = tpCount/samplingFrequency
    frequencies = values/timePeriod
    return frequencies, fourierTransform


samplingFrequency = 100
samplingInterval = 1 / samplingFrequency
beginTime = 0
endTime = 10
signal1Frequency = 4
signal2Frequency = 7
amplitudes = []
time = np.arange(beginTime, endTime, samplingInterval)
amplitudes.append(5*np.sin(2*np.pi*signal1Frequency*time))
amplitudes.append(np.sin(2*np.pi*signal2Frequency*time))
amplitudes.append(np.sin(2*np.pi*0.5*time))

amplitude = 0
for x in amplitudes:
    amplitude += x
transform = fourier_transform(amplitude)  # amplitude is f(t) where f(t) = asin(bx) + csin(cx) + esin(dx)...
print(transform[1], transform[0])
figure, axis = plotter.subplots(2, 1)
plotter.subplots_adjust(hspace=1)
axis[0].set_title('Original')
axis[0].plot(time, amplitude)
axis[1].set_title('Fourier transform depicting the frequency components')
axis[1].plot(transform[0], abs(transform[1]))
axis[1].set_xlabel('Frequency')
axis[1].set_ylabel('Likelyness')
plotter.show()  # this is all just for visualising whats happening
