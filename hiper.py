import numpy as np


def hiper(muestras, x, xA, fs):
    c = 340  # speed of sound
    pot = 2*np.ones((1, int(len(x))))
    dist = (muestras*c)/fs  # distance to b'
    y1 = np.sqrt((dist**2/4-xA**2+(4*xA**2/dist**2-1)*x**pot))  # Formula with the requisitions xA = -xB and yA = yB = 0
    return y1
